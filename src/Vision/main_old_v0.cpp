/*
 * main_v0.cpp
 *
 *  Created on: Aug 3, 2017
 *      Author: iveta
 */


#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>
//#include <opencv2/imgproc.hpp>
//#include <opencv2/imgcodecs.hpp>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

//#include <ros/ros.h>
//#include <sensor_msgs/Image.h>
//#include <std_msgs/Int32MultiArray.h>
//#include <std_msgs/Float64MultiArray.h>
//#include <message_filters/subscriber.h>
//#include <message_filters/synchronizer.h>
//#include <message_filters/sync_policies/approximate_time.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

using namespace cv;
using namespace std;


int main()
{
	//set up camera and start streaming//

	// Create a context object. This object owns the handles to all connected realsense devices
	rs::context ctx;

	// Access the first available RealSense device
	rs::device * dev = ctx.get_device(0);
	printf("\nUsing device 0, an %s\n", dev->get_name());
	printf("    Serial number: %s\n", dev->get_serial());
	printf("    Firmware version: %s\n", dev->get_firmware_version());

	// Configure color stream to run at VGA resolution at 30 frames per second
	dev->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 30);

	// Start streaming
	dev->start();

	// Camera warm-up - Dropped several first frames to let auto-exposure stabilize
	for(int i = 0; i < 30; i++){
		dev->wait_for_frames();
	}

	//take an image
	cout << "First RGB image will be taken:" << endl;
	Mat color1(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);
	cout << "First RGB image has been taken." << endl;

	namedWindow("image1",CV_WINDOW_AUTOSIZE);
	imshow("image1",color1);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image1.png",color1);
	waitKey(0);
	destroyWindow("image1");


	//take a second image
/*
	cout << "Second RGB image will be taken:" << endl;
	for(int i = 0; i < 30; i++){
			dev->wait_for_frames();
		}
	Mat color2(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);
	cout << "Second RGB image has been taken." << endl;

	namedWindow("image2",CV_WINDOW_AUTOSIZE);
	imshow("image2",color2);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image2.png",color2);
	waitKey(0);
	destroyWindow("image2");
*/

	dev->stop();




	//--Step 1: Keypoint detection

	SiftFeatureDetector detector;
	std::vector<cv::KeyPoint> keypoints1, keypoints2;
	detector.detect(color1, keypoints1);
	//detector.detect(color2, keypoints2);

	// Add results to image and save.
	Mat output1, output2;
	drawKeypoints(color1, keypoints1, output1);
	//drawKeypoints(color2, keypoints2, output2);
	namedWindow("sift_keypoints1",CV_WINDOW_AUTOSIZE);
	imshow("sift_keypoints1",output1);
	imwrite("sift_keypoints1.jpg", output1);
	waitKey(0);

	//namedWindow("sift_keypoints2",CV_WINDOW_AUTOSIZE);
	//imshow("sift_keypoints2",output2);
	//imwrite("sift_keypoints2.jpg", output2);
	//waitKey(0);


	//-- Step 2: Feature extraction


	Mat descriptors1, descriptors2;
	SiftDescriptorExtractor extractor;
	extractor.compute(color1, keypoints1, descriptors1);
	//extractor.compute(color2, keypoints2, descriptors2);
	printf("Image 1:%zd keypoints are found.\n", keypoints1.size());
	//printf("Image 2:%zd keypoints are found.\n", keypoints2.size());

/*
	FileStorage fs("/home/iveta/realsense_ws/src/comicbot/descriptors/descriptors2.yml", FileStorage::WRITE);
	write(fs, "keypoints2", keypoints1);
	write(fs, "descriptors2", descriptors1);
	fs.release();
*/

	Mat temp;
	vector<KeyPoint> temp2;

	FileStorage fs("/home/iveta/realsense_ws/src/comicbot/descriptors/descriptors2.yml", FileStorage::READ);
	fs["descriptors2"] >> descriptors2;
	FileNode kptNode = fs["keypoints2"];
	read(kptNode, keypoints2);

	fs.release();



	//-- Step 3: Matching descriptor vectors using FLANN matcher

	FlannBasedMatcher matcher;
	std::vector< DMatch > matches;
	matcher.match(descriptors1, descriptors2, matches);


	//Calculation of max and min distances between keypoints
	double max_dist = 0; double min_dist = 100;
	for( int i = 0; i < descriptors1.rows; i++ ){
		 double dist = matches[i].distance;
	 	 if( dist < min_dist ) min_dist = dist;
	 	 if( dist > max_dist ) max_dist = dist;
	}

	printf("-- Max dist : %f \n", max_dist );
	printf("-- Min dist : %f \n", min_dist );

	//-- Step4: Draw only "good" matches
	std::vector< DMatch > good_matches;
	for( int i = 0; i < descriptors1.rows; i++ ){
		if( matches[i].distance <= max(2*min_dist, 0.02) ){
			 good_matches.push_back(matches[i]);
		}
	}

	Mat color2=imread("/home/iveta/realsense_ws/src/comicbot/images/image2.png",1);
	Mat img_matches;
	drawMatches(color1, keypoints1, color2, keypoints2, good_matches, img_matches,
			 Scalar::all(-1), Scalar::all(-1),vector<char>(),
			 DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

	//Show detected matches
	namedWindow("good_matches",CV_WINDOW_AUTOSIZE);
	imshow( "good_matches", img_matches );
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/good_matches.png",img_matches);
	waitKey(0);



	return 0;
}





//convert to greyscale
	/*
	Mat grey;
	cv::cvtColor(color, grey, cv::COLOR_BGR2GRAY);
	namedWindow("image_greyscale",CV_WINDOW_AUTOSIZE);
	imshow("image_greyscale",grey1);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/mage_greyscale.png",grey1);
	waitKey(0);
	*/



