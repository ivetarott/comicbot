/*
 * extract_comics_descriptors.cpp
 *
 *  Created on: Aug 3, 2017
 *      Author: iveta
 */

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>


#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

using namespace cv;
using namespace std;

const int NUMBER_PAGES=51;

int main(){


	SiftFeatureDetector detector;
	SiftDescriptorExtractor extractor;
	FileStorage fs("/home/iveta/realsense_ws/src/comicbot/descriptors/comics_descriptors.yml", FileStorage::WRITE);

	for(int i=1; i<= NUMBER_PAGES; i++){

		Mat img;

		std::ostringstream os;
		os << std::setw( 4 ) << std::setfill( '0' ) << i;
		string filename = "/home/iveta/realsense_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + ".jpg";
		img= imread(filename,1);

		// Check for invalid input
		if(! img.data ){
			cout <<  "Could not open or find the image "<< i << "." << std::endl ;

		}
		else {
			/*
			//compression of the scanned image
			filename = "/home/iveta/realsense_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + "_compressed.jpg";
			bool success= imwrite(filename,img,{ CV_IMWRITE_JPEG_QUALITY,10});
			if (success){
				cout <<  "Compression of image "<< i << " successful." << std::endl ;
			}
			img= imread(filename,1);
			*/

			//resize the scanned image
			resize(img, img, Size(), 0.05, 0.05, CV_INTER_AREA);
			filename = "/home/iveta/realsense_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + "_compressed.jpg";
			bool success= imwrite(filename,img);
			if (success){
				cout <<  "Compression of image "<< i << " successful." << std::endl ;
			}
			//keypoint detection
			vector<KeyPoint> keypoints;
			detector.detect(img, keypoints);
			Mat output;

			//save the image with keypoints for further reference
			drawKeypoints(img, keypoints, output);
			string iValue = to_string(i);
			filename = "/home/iveta/realsense_ws/src/comicbot/images/comics_keypoints/image_" + iValue + "_keypoints.png";
			imwrite(filename, output);

			//feature extraction
			Mat descriptors;
			extractor.compute(img, keypoints, descriptors);
			printf("Image %d:%zd keypoints were found.\n", i, keypoints.size());

			//write keypoints descriptors to a file
			filename = "keypoints_" + iValue;
			string filename2 = "descriptors_" + iValue;
			write(fs, filename, keypoints);
			write(fs, filename2, descriptors);
			//fs.release();
		}

	}
	fs.release();

	return 0;
}


