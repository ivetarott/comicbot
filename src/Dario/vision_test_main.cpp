#include <opencv2/opencv.hpp>
//#include "imgcodecs.hpp"
//#include "highgui.hpp"
//#include "imgproc.hpp"

#include <iostream>


// include the librealsense C++ header file
#include <librealsense/rs.hpp>


using namespace cv;
using namespace std;



int main()
{



    Mat src1, src2, src3, src4;


//code from the tutorial








   // Create a context object. This object owns the handles to all connected realsense devices
    rs::context ctx;

    // Access the first available RealSense device
    rs::device * dev = ctx.get_device(0);

    // Configure Infrared stream to run at VGA resolution at 30 frames per second
    dev->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 30);

    // Start streaming
    dev->start();

    // Camera warmup - Dropped several first frames to let auto-exposure stabilize
    for(int i = 0; i < 30; i++)
       dev->wait_for_frames();

    // Creating OpenCV Matrix from a color image
    Mat color(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);

    // Display in a GUI
    namedWindow("Display Image", WINDOW_AUTOSIZE );
    imshow("Display Image", color);

    Mat src = color;

    waitKey(0);

  // Creating OpenCV Matrix from a color image
std::cout << __LINE__ << __FILE__ <<std::endl;
   for(int i = 0; i < 30; i++)
       dev->wait_for_frames();
    Mat color2(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Display in a GUI
    //namedWindow("Display Image", WINDOW_AUTOSIZE );
    imshow("Display Image", color2);

    src2 = color2;
    
std::cout << __LINE__ << __FILE__ <<std::endl;
    waitKey(0);
















std::cout << __LINE__ << __FILE__ <<std::endl;
    GaussianBlur(src1,src1,Size(3,3),0,0);
    GaussianBlur(src2,src2,Size(3,3),0,0);
std::cout << __LINE__ << __FILE__ <<std::endl;


    absdiff(src1,src2,src3);
    //cvtColor(src33,src3_grey,CV_RGB2GRAY);

    imshow("foreground",src1);
    imshow("background",src2);
    imshow("difference",src3);

//    Mat kernel = Mat::ones(3, 3, CV_8UC1);
//    morphologyEx(src3,src3, MORPH_OPEN, kernel);


//    dilate(src3, src3, kernel);
  GaussianBlur(src3,src3,Size(3,3),0,0);

//  imshow("difference denoised",src3);

    Mat blur, norm;
    normalize(src3,norm, 0, 255, NORM_MINMAX, CV_8UC1);
//    GaussianBlur(norm,blur,Size(15,15),0,0);
//    imshow("no gaussian",src33);
//      imshow("gaussian",blur);

    imshow("normalized",norm);

    cvtColor(src3,src3,COLOR_GRAY2BGR);






//cvtColor(norm,norm,COLOR_GRAY2BGR);


//    // Create a kernel that we will use for accuting/sharpening our image
//    Mat kernel = (Mat_<float>(3,3) <<
//            1,  1, 1,
//            1, -8, 1,
//            1,  1, 1); // an approximation of second derivative, a quite strong kernel
//    // do the laplacian filtering as it is
//    // well, we need to convert everything in something more deeper then CV_8U
//    // because the kernel has some negative values,
//    // and we can expect in general to have a Laplacian image with negative values
//    // BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
//    // so the possible negative number will be truncated
//    Mat imgLaplacian;
//    Mat sharp = norm; // copy source image to another temporary one
//    filter2D(sharp, imgLaplacian, CV_32F, kernel);
//    norm.convertTo(sharp, CV_32F);
//    Mat imgResult = sharp - imgLaplacian;
//    // convert back to 8bits gray scale
//    imgResult.convertTo(imgResult, CV_8UC3);
//    imgLaplacian.convertTo(imgLaplacian, CV_8UC3);
//    // imshow( "Laplace Filtered Image", imgLaplacian );
//    imshow( "New Sharped Image", imgResult );
//    norm = imgResult; // copy back


//    cvtColor(norm, norm, CV_BGR2GRAY);











    threshold(norm,src4,0,255,THRESH_BINARY | CV_THRESH_OTSU);

//    adaptiveThreshold(norm, src4, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 21,0);

    imshow("otsu",src4);

      GaussianBlur(src4,src4,Size(3,3),0,0);
    imshow("otsu after gauss",src4);


    // Perform the distance transform algorithm
    Mat dist;
    distanceTransform(src4, dist, CV_DIST_L2, 3);
    // Normalize the distance image for range = {0.0, 1.0}
    // so we can visualize and threshold it
    normalize(dist, dist, 0, 1., NORM_MINMAX);
    imshow("Distance Transform Image", dist);
    // Threshold to obtain the peaks
    // This will be the markers for the foreground objects
    //threshold(dist, dist, .1, 1., CV_THRESH_BINARY);


    // Dilate a bit the dist image
    Mat kernel1 = Mat::ones(3, 3, CV_8UC1);
    dilate(dist, dist, kernel1);
    imshow("Peaks", dist);
    // Create the CV_8U version of the distance image
    // It is needed for findContours()
    Mat dist_8u;
//    src4.convertTo(dist_8u, CV_8U);
    dist.convertTo(dist_8u, CV_8U);






    // Find total markers
    vector<vector<Point> > contours;
    findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    // Create the marker image for the watershed algorithm
    Mat markers = Mat::zeros(dist.size(), CV_32SC1);
    // Draw the foreground markers
    for (size_t i = 0; i < contours.size(); i++)
        drawContours(markers, contours, static_cast<int>(i), Scalar::all(static_cast<int>(i)+1), -1);
    // Draw the background marker
    circle(markers, Point(5,5), 3, CV_RGB(255,255,255), -1);
    imshow("Markers", markers*10000);
    // Perform the watershed algorithm


    cvtColor(src4,src4,COLOR_GRAY2BGR);




    watershed(src4, markers);
    Mat mark = Mat::zeros(markers.size(), CV_8UC1);
    markers.convertTo(mark, CV_8UC1);
    bitwise_not(mark, mark);
    imshow("Markers_v2", mark); // uncomment this if you want to see how the mark
                                  // image looks like at that point
    // Generate random colors
    vector<Vec3b> colors;
    for (size_t i = 0; i < contours.size(); i++)
    {
        int b = theRNG().uniform(0, 255);
        int g = theRNG().uniform(0, 255);
        int r = theRNG().uniform(0, 255);
        colors.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }
    // Create the result image
    Mat dst = Mat::zeros(markers.size(), CV_8UC3);
    // Fill labeled objects with random colors
    for (int i = 0; i < markers.rows; i++)
    {
        for (int j = 0; j < markers.cols; j++)
        {
            int index = markers.at<int>(i,j);
            if (index > 0 && index <= static_cast<int>(contours.size()))
                dst.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else
                dst.at<Vec3b>(i,j) = Vec3b(0,0,0);
        }
    }
    // Visualize the final image

  cvtColor(mark,mark,COLOR_GRAY2BGR);


//    GaussianBlur(mark,dst,Size(21,21),0,0);
    GaussianBlur(dist,dist,Size(21,21),0,0);


    imshow("Final Result", dst);





    cvtColor(dst,dst,COLOR_BGR2GRAY);
    cvtColor(src4,src4,COLOR_BGR2GRAY);




    int thresh=200;

    Mat srccorner, graycorner;
    // Load source image and convert it to gray
    srccorner = dist;
//    cvtColor( srccorner, graycorner, CV_BGR2GRAY );
    Mat dst_corner, dst_norm_corner, dst_norm_scaled_corner;
    dst_corner = Mat::zeros( srccorner.size(), CV_32FC1 );

    // Detecting corners
    cornerHarris( srccorner, dst_corner, 7, 5, 0.05, BORDER_DEFAULT );

    // Normalizing
    normalize( dst_corner, dst_norm_corner, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
    convertScaleAbs( dst_norm_corner, dst_norm_scaled_corner );





    Mat cornerdet,edgesimg;
    cornerdet = Mat::zeros( srccorner.size(), CV_32FC1 );
    edgesimg= Mat::zeros( srccorner.size(), CV_32FC1 );

    // Drawing a circle around corners
    int maxrow=0;
    int mincol=0;
    float distanceleft=dst_norm_scaled_corner.rows;
    float distancelefttry;
    float distanceright=dst_norm_scaled_corner.cols;
    float distancerighttry;
    int rightcol;
    int rightrow;
    for( int j = 0; j < dst_norm_corner.rows ; j++ )
    { for( int i = 0; i < dst_norm_corner.cols; i++ )
    {
        if( (int) dst_norm_corner.at<float>(j,i) > thresh )
        {
            circle( cornerdet, Point( i, j), 10,  Scalar(150), 2, 8, 0 );
             distancelefttry=sqrt(pow((dst_norm_corner.rows-j),2)+pow(i,2));
             distancerighttry=sqrt(pow(dst_norm_corner.rows-j,2)+pow((dst_norm_corner.cols-i),2));

            if (distancelefttry<distanceleft){
                maxrow=j;
                mincol=i;
                distanceleft=distancelefttry;
            }
            if (distancerighttry<distanceright){
               rightrow=j;
               rightcol=i;
               distanceright=distancerighttry;
            }

        }
    }
    }



    circle( dst_norm_scaled_corner, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( rightcol, rightrow ), 50,  Scalar(255), 2, 8, 0 );



    // Showing the result
//    namedWindow( "corners_window", CV_WINDOW_AUTOSIZE );
    imshow( "corners_window", dst_norm_scaled_corner);
    imshow("cornerdet",cornerdet);
    imshow("corners",edgesimg);



    waitKey();

    return 0;
}
