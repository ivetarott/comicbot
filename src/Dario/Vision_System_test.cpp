#include <opencv2/opencv.hpp>
//#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc.hpp>

#include <iostream>


// include the librealsense C++ header file
//#include <ros/ros.h>
#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>


#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Float64MultiArray.h>


//#include <message_filters/subscriber.h>
//#include <message_filters/synchronizer.h>
#include <sensor_msgs/Image.h>
//#include <message_filters/sync_policies/approximate_time.h>
#include <keyboard/Key.h>

#include <eigen3/Eigen/Dense>

#include <math.h>




using namespace cv;
using namespace std;



int main()
{

	double depthvalue;
	double depthvalue_right;

	sensor_msgs::Image image;
	sensor_msgs::Image image_depth;
	keyboard::Key value;

//camera intrinsics
  	float alpha_x;
  	float alpha_y;
  	float u_0;
	float v_0;
	float gamma;
	Eigen::Matrix3f K;
	  
	Eigen::Matrix3f test;
	float turningangle;
	float x_shift;
	float y_shift;
	float z_shift;

	float comicorientation;
	float comicwidth;
	float leftspot_x;
	float leftspot_y;
	float comicmargin;


	float quaternion_x;
	float quaternion_y;
	float quaternion_z;
	float quaternion_w;

	float baseline_rotation;

  	float baseline;

  	float wrist_yaw_orientation;

  	float handcorrection_x;
  	float handcorrection_y;

  	float testterm;

  	Eigen::Matrix4f Shiftmatrix;
  	Eigen::Vector3f camerapositions;
  	Eigen::Vector3f camerapositions_transformed;
  	Eigen::Vector4f camerapositions_homogenious;

  	Eigen::Vector3f camerapositions_right;
  	Eigen::Vector3f camerapositions_right_transformed;
  	Eigen::Vector4f camerapositions_right_homogenious;

  	Eigen::Vector4f cornerworldframe;
  	Eigen::Vector4f cornerworldframe_right;

	


//set up camera and start streaming

 	    // Create a context object. This object owns the handles to all connected realsense devices
	    rs::context ctx;

	    // Access the first available RealSense device
	    rs::device * dev = ctx.get_device(0);
	    printf("\nUsing device 0, an %s\n", dev->get_name());
            printf("    Serial number: %s\n", dev->get_serial());
    	    printf("    Firmware version: %s\n", dev->get_firmware_version());

	    // Configure Infrared stream to run at VGA resolution at 30 frames per second
	    dev->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 30);
	    dev->enable_stream(rs::stream::depth, 640, 480, rs::format::z16, 30);
    	    dev->enable_stream(rs::stream::infrared, 640, 480, rs::format::y8, 30);

	    // Start streaming
	    dev->start();

	    // Camera warmup - Dropped several first frames to let auto-exposure stabilize
	    for(int i = 0; i < 30; i++){
	       dev->wait_for_frames();
	    }
//take a background image
	std::cout << "Background image will be taken:" << std::endl;
	//cv::waitKey(0);
	Mat src1(Size(640, 480), CV_8UC1, (void*)dev->get_frame_data(rs::stream::infrared), Mat::AUTO_STEP);
	std::cout << "Background image has been taken:" << std::endl;
	//Dario's code:
        /*
        cv_bridge::CvImagePtr cvimg = cv_bridge::toCvCopy(image);
        cv::Mat img_conv;
        cvimg->image.convertTo(img_conv,CV_8UC1,0.03);
        src1=img_conv;
	*/

        namedWindow("Background Image",CV_WINDOW_AUTOSIZE);
        imshow("Background Image",src1);
	cv::waitKey(0);
        imwrite("/home/iveta/realsense_ws/src/comicbot/images/BackgroundImage.png",src1);

			

//take a foreground image

        std::cout << "Foreground image will be taken:" << std::endl;
	//cv::waitKey(0);	
	
	//Dario's code:	
	/* 
	cv_bridge::CvImagePtr cvimg2 = cv_bridge::toCvCopy(image);
        cv_bridge::CvImagePtr cvimg2_depth = cv_bridge::toCvCopy(image_depth);
        Mat img2_conv;
        Mat img2_conv_depth;
        cvimg2->image.convertTo(img2_conv,CV_8UC1,0.03);
        cvimg2_depth->image.convertTo(img2_conv_depth,CV_16U);
        src2=img2_conv;
        depth=img2_conv_depth;
	*/

        for(int i = 0; i < 30; i++){
	     dev->wait_for_frames();
        }
	Mat src2(Size(640, 480), CV_8UC1, (void*)dev->get_frame_data(rs::stream::infrared), Mat::AUTO_STEP);
	Mat depth(Size(640, 480), CV_16U, (void*)dev->get_frame_data(rs::stream::depth), Mat::AUTO_STEP);
	std::cout << "Foreground image has been taken:" << std::endl;

        Mat imgsave;
        cvtColor(src2,imgsave,COLOR_GRAY2BGR);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/greyscale.png",imgsave);
	namedWindow("Greyscale",CV_WINDOW_AUTOSIZE);
	imshow("Greyscale",imgsave);
        imwrite("/home/iveta/realsense_ws/src/comicbot/images/ForegroundImage.png",src2);
        imwrite("/home/iveta/realsense_ws/src/comicbot/images/DepthImage.png",depth);

        namedWindow("Foreground Image",CV_WINDOW_AUTOSIZE);
        namedWindow("Depth Image",CV_WINDOW_AUTOSIZE);
        imshow("Foreground Image",src2);
        imshow("Depth Image",depth);

//image analysis

        std::cout << "Image Analysis will be performed:" << std::endl;


	cv::GaussianBlur(src1,src1,Size(3,3),0,0);
        cv::GaussianBlur(src2,src2,Size(3,3),0,0);

	imwrite("/home/iveta/realsense_ws/src/comicbot/images/GaussianBlur1.png",src1);
	namedWindow("GaussianBlur1",CV_WINDOW_AUTOSIZE);
        imshow("GaussianBlur1", src1);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/GaussianBlur2.png",src2);
	namedWindow("GaussianBlur2",CV_WINDOW_AUTOSIZE);
        imshow("GaussianBlur2", src2);

        Mat src3, src4;
        absdiff(src1,src2,src3);

        imwrite("/home/iveta/realsense_ws/src/comicbot/images/DifferenceImage.png",src3);

        namedWindow("Difference Image",CV_WINDOW_AUTOSIZE);
        imshow("Difference Image", src3);
        
        GaussianBlur(src3,src3,Size(3,3),0,0);

   	Mat blur, norm;
    	normalize(src3,norm, 0, 255, NORM_MINMAX, CV_8UC1);
	namedWindow("normalized",CV_WINDOW_AUTOSIZE);
    	imshow("normalized",norm);

    	cvtColor(src3,src3,COLOR_GRAY2BGR);



    threshold(norm,src4,0,255,THRESH_BINARY | CV_THRESH_OTSU);

    imshow("otsu",src4);

    GaussianBlur(src4,src4,Size(3,3),0,0);

    imwrite("/home/iveta/realsense_ws/src/comicbot/images/OtsuImage.png",src4);

    namedWindow("Otsu Image",CV_WINDOW_AUTOSIZE);
  
    imshow("Otsu Image",src4);


    // Perform the distance transform algorithm
    Mat dist;
    distanceTransform(src4, dist, CV_DIST_L2, 3);
    // Normalize the distance image for range = {0.0, 1.0}
    // so we can visualize and threshold it
    normalize(dist, dist, 0, 1., NORM_MINMAX);
    // imshow("Distance Transform Image", dist);
    // Threshold to obtain the peaks
    // This will be the markers for the foreground objects
    //threshold(dist, dist, .1, 1., CV_THRESH_BINARY);


    // Dilate a bit the dist image
    Mat kernel1 = Mat::ones(3, 3, CV_8UC1);
    dilate(dist, dist, kernel1);


    imwrite("/home/iveta/realsense_ws/src/comicbot/images/DistanceTransform.png",dist);

    namedWindow("Distance Transform",CV_WINDOW_AUTOSIZE);
  
    imshow("Distance Transform",dist);
    // imshow("Peaks", dist);
    // Create the CV_8U version of the distance image
    // It is needed for findContours()
    Mat dist_8u;
    src4.convertTo(dist_8u, CV_8U);





    // Find total markers
    vector<vector<Point> > contours;
    findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    // Create the marker image for the watershed algorithm
    Mat markers = Mat::zeros(dist.size(), CV_32SC1);
    // Draw the foreground markers
    for (size_t i = 0; i < contours.size(); i++)
        drawContours(markers, contours, static_cast<int>(i), Scalar::all(static_cast<int>(i)+1), -1);
    // Draw the background marker
    circle(markers, Point(5,5), 3, CV_RGB(255,255,255), -1);
    // imshow("Markers", markers*10000);
    // Perform the watershed algorithm


    cvtColor(src4,src4,COLOR_GRAY2BGR);




    watershed(src4, markers);
    Mat mark = Mat::zeros(markers.size(), CV_8UC1);
    markers.convertTo(mark, CV_8UC1);
    bitwise_not(mark, mark);
    // imshow("Markers_v2", mark); // uncomment this if you want to see how the mark
                                  // image looks like at that point
    // Generate random colors
    vector<Vec3b> colors;
    for (size_t i = 0; i < contours.size(); i++)
    {
        int b = theRNG().uniform(0, 255);
        int g = theRNG().uniform(0, 255);
        int r = theRNG().uniform(0, 255);
        colors.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Create the result image
    Mat dst = Mat::zeros(markers.size(), CV_8UC3);
    // Fill labeled objects with random colors
    for (int i = 0; i < markers.rows; i++)
    {
        for (int j = 0; j < markers.cols; j++)
        {
            int index = markers.at<int>(i,j);
            if (index > 0 && index <= static_cast<int>(contours.size()))
                dst.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else
                dst.at<Vec3b>(i,j) = Vec3b(0,0,0);
        }
    }
    // Visualize the final image
std::cout << __LINE__ << __FILE__ <<std::endl;
    cvtColor(mark,mark,COLOR_GRAY2BGR);


    GaussianBlur(mark,dst,Size(21,21),0,0);
    GaussianBlur(dist,dist,Size(21,21),0,0);
std::cout << __LINE__ << __FILE__ <<std::endl;
    imwrite("/home/iveta/realsense_ws/src/comicbot/images/FinalResult.png",dst);

    namedWindow("Final Result",CV_WINDOW_AUTOSIZE);

    imshow("Final Result", dst);
std::cout << __LINE__ << __FILE__ <<std::endl;





    cvtColor(dst,dst,COLOR_BGR2GRAY);
    cvtColor(src4,src4,COLOR_BGR2GRAY);




    int thresh=200;

    Mat srccorner, graycorner;
    // Load source image and convert it to gray
    srccorner = dist;
//    cvtColor( srccorner, graycorner, CV_BGR2GRAY );
    Mat dst_corner, dst_norm_corner, dst_norm_scaled_corner;
    dst_corner = Mat::zeros( srccorner.size(), CV_32FC1 );
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Detecting corners
    cornerHarris( srccorner, dst_corner, 7, 5, 0.05, BORDER_DEFAULT );

    // Normalizing
    normalize( dst_corner, dst_norm_corner, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
    convertScaleAbs( dst_norm_corner, dst_norm_scaled_corner );
std::cout << __LINE__ << __FILE__ <<std::endl;




    Mat cornerdet,edgesimg;
    cornerdet = Mat::zeros( srccorner.size(), CV_32FC1 );
    edgesimg= Mat::zeros( srccorner.size(), CV_32FC1 );
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Drawing a circle around corners
    int maxrow=0;
    int mincol=0;
    float distanceleft=dst_norm_scaled_corner.rows;
    float distancelefttry;
    float distanceright=dst_norm_scaled_corner.cols;
    float distancerighttry;
    int rightcol;
    int rightrow;
    for( int j = 0; j < dst_norm_corner.rows ; j++ )
    { for( int i = 0; i < dst_norm_corner.cols; i++ )
    {
        if( (int) dst_norm_corner.at<float>(j,i) > thresh )
        {
            circle( cornerdet, Point( i, j), 10,  Scalar(150), 2, 8, 0 );
             distancelefttry=sqrt(pow((dst_norm_corner.rows-j),2)+pow(i,2));
             distancerighttry=sqrt(pow(dst_norm_corner.rows-j,2)+pow((dst_norm_corner.cols-i),2));

            if (distancelefttry<distanceleft){
                maxrow=j;
                mincol=i;
                distanceleft=distancelefttry;
            }
            if (distancerighttry<distanceright){
               rightrow=j;
               rightcol=i;
               distanceright=distancerighttry;
            }

        }
    }
    }
std::cout << __LINE__ << __FILE__ <<std::endl;


    circle( dst_norm_scaled_corner, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( rightcol, rightrow ), 50,  Scalar(255), 2, 8, 0 );
std::cout << __LINE__ << __FILE__ <<std::endl;


    imwrite("/home/iveta/realsense_ws/src/comicbot/images/CornerCadidates.png",cornerdet);
    imwrite("/home/iveta/realsense_ws/src/comicbot/images/CornerResults.png",edgesimg);


    // Showing the result
//    namedWindow( "corners_window", CV_WINDOW_AUTOSIZE );
    // imshow( "corners_window", dst_norm_scaled_corner);
    namedWindow("Corner Candidates",CV_WINDOW_AUTOSIZE);
    namedWindow("Corner Result",CV_WINDOW_AUTOSIZE);
    cv::waitKey(0);
    imshow("Corner Candidates",cornerdet);
    imshow("Corner Result",edgesimg);
std::cout << __LINE__ << __FILE__ <<std::endl;  
    depthvalue=static_cast<float>(depth.at<unsigned short>(maxrow,mincol));
std::cout << __LINE__ << __FILE__ <<std::endl;  
    depthvalue=depthvalue/1000;
std::cout << __LINE__ << __FILE__ <<std::endl;  
    depthvalue_right=static_cast<float>(depth.at<unsigned short>(rightrow,rightcol));
    depthvalue_right=depthvalue_right/1000;
std::cout << __LINE__ << __FILE__ <<std::endl;  
/*
    camerapositions(0)=mincol;
    camerapositions(1)=maxrow;
    camerapositions(2)=1;

    camerapositions_right(0)=rightcol;
    camerapositions_right(1)=rightrow;
    camerapositions_right(2)=1;
    // K=K.inverse();
    camerapositions_transformed=depthvalue*K.inverse()*camerapositions;
    test=K.inverse();

    camerapositions_right_transformed=depthvalue_right*K.inverse()*camerapositions_right;

    // camerapositions_transformed=depthvalue*camerapositions;

    camerapositions_homogenious(0)=camerapositions_transformed(0);
    camerapositions_homogenious(1)=camerapositions_transformed(1);
    camerapositions_homogenious(2)=camerapositions_transformed(2);
    camerapositions_homogenious(3)=1;

    camerapositions_right_homogenious(0)=camerapositions_right_transformed(0);
    camerapositions_right_homogenious(1)=camerapositions_right_transformed(1);
    camerapositions_right_homogenious(2)=camerapositions_right_transformed(2);
    camerapositions_right_homogenious(3)=1;


    cornerworldframe=Shiftmatrix*camerapositions_homogenious;

    cornerworldframe_right=Shiftmatrix*camerapositions_right_homogenious;



    baseline=0.055;

    testterm=(cornerworldframe_right(0)-cornerworldframe(0))/abs(cornerworldframe(1)-cornerworldframe_right(1));
    comicorientation=atan2((cornerworldframe_right(0)-cornerworldframe(0)),abs(cornerworldframe(1)-cornerworldframe_right(1)));
    comicwidth=sqrt(pow((camerapositions_right_homogenious(0)-camerapositions_homogenious(0)),2)+pow((camerapositions_right_homogenious(1)-camerapositions_homogenious(1)),2));
    



    wrist_yaw_orientation=(baseline_rotation/180)*3.14159+comicorientation;



    leftspot_x=-cos(comicorientation)*comicmargin+sin(comicorientation)*(comicwidth/4-baseline/2);
    leftspot_y=-sin(comicorientation)*comicmargin-cos(comicorientation)*(comicwidth/4-baseline/2);

    handcorrection_x=cos(wrist_yaw_orientation)*0.095+sin(wrist_yaw_orientation)*0.005;
    handcorrection_y=sin(wrist_yaw_orientation)*0.095-cos(wrist_yaw_orientation)*0.005;

    handcorrection_x=0.095-handcorrection_x;
    handcorrection_y=-0.005-handcorrection_y;

    leftspot_x=leftspot_x+handcorrection_x;
    leftspot_y=leftspot_y+handcorrection_y;

    quaternion_x=0;
    quaternion_y=0;
    quaternion_z=sin(wrist_yaw_orientation/2);
    quaternion_w=cos(wrist_yaw_orientation/2);




    std_msgs::Float64MultiArray cornerpositions;
    cornerpositions.data.clear();
    cornerpositions.data.resize(27);

    // cornerpositions.data=cornerworldframe;
    
    cornerpositions.data[0]=cornerworldframe(0);
    cornerpositions.data[1]=cornerworldframe(1);
    cornerpositions.data[2]=cornerworldframe(2);
    cornerpositions.data[3]=cornerworldframe_right(0);
    cornerpositions.data[4]=cornerworldframe_right(1);
    cornerpositions.data[5]=cornerworldframe_right(2);
    cornerpositions.data[6]=camerapositions_transformed(0);
    cornerpositions.data[7]=camerapositions_transformed(1);
    cornerpositions.data[8]=camerapositions_transformed(2);
    cornerpositions.data[9]=depthvalue;
    cornerpositions.data[10]=depthvalue_right;
    cornerpositions.data[11]=comicorientation;
    cornerpositions.data[12]=comicwidth;
    cornerpositions.data[13]=leftspot_x;
    cornerpositions.data[14]=leftspot_y;
    cornerpositions.data[15]=wrist_yaw_orientation;
    cornerpositions.data[16]=quaternion_z;
    cornerpositions.data[17]=quaternion_w;
    cornerpositions.data[18]=mincol;
    cornerpositions.data[19]=maxrow;
    cornerpositions.data[20]=sin(turningangle);
    cornerpositions.data[21]=turningangle;
    cornerpositions.data[22]=Shiftmatrix(0,1);
    cornerpositions.data[23]=comicorientation;
    cornerpositions.data[24]=testterm;
    cornerpositions.data[25]=handcorrection_x;
    cornerpositions.data[26]=handcorrection_y;

*/
std::cout << __LINE__ << __FILE__ <<std::endl;
        destroyWindow("Difference Image");
        destroyWindow("Otsu Image");
        destroyWindow("Final Result");
        destroyWindow("Corner Candidates");
        destroyWindow("Corner Result");
        destroyWindow("Background Image");
        destroyWindow("Foreground Image");
        destroyWindow("Depth Image");
	destroyWindow("Greyscale");




	return 0;
}
