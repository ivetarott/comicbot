#include <ros/ros.h>
#include <image_transport/image_transport.h>
// #include <opencv2/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <librealsense/rs.hpp>


#include <opencv2/opencv.hpp>
// #include <opencv2/imgcodecs.hpp>
// #include <opencv2/imgproc.hpp>

#include <iostream>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Float64MultiArray.h>



#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <sensor_msgs/Image.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <keyboard/Key.h>

#include <eigen3/Eigen/Dense>

#include <math.h>

using namespace cv;
using namespace std;



class SubscribeAndPublish
{
public:
    SubscribeAndPublish()
    :
    image_sub (nh,"camera/ir/image_raw",1),
    depth_sub (nh,"camera/depth/image_raw",1),
    key_sub (nh,"keyboard/keydown",1),
    sync(MySyncPolicy(10), image_sub,depth_sub,key_sub),
    Shiftmatrix(4,4),
    K(3,3)


    {
        //do something
        positionpub=nh.advertise<std_msgs::Float64MultiArray>("cornerspots",1);
        sync.registerCallback(boost::bind(&SubscribeAndPublish::callback, this, _1, _2, _3));
        alpha_x=468.29773507379593;
        alpha_y=467.0979149631861;
        u_0=324.555009323928;
        v_0=240.684513692166;
        gamma=0;
        K << alpha_x, gamma, u_0,
             0, alpha_y, v_0,
             0,0,1;
  // K.at<float>(0,0)=alpha_x;
  // K.at<float>(0,1)=gamma;
  // K.at<float>(0,2)=u_0;
  // K.at<float>(1,0)=0;
  // K.at<float>(1,1)=alpha_y;
  // K.at<float>(1,2)=v_0;
  // K.at<float>(2,0)=0;
  // K.at<float>(2,1)=0;
  // K.at<float>(2,2)=0;
        turningangle=-2.0594885173533;
        // turningangle=(120/360)*2*3.14159;
        x_shift=0.067;
        y_shift=-0.174;
        z_shift=0.225;

        baseline_rotation=28.61;
        comicmargin=0.11;



        Shiftmatrix <<  0,cos(turningangle),-sin(turningangle), x_shift,
                        -1,0,0,y_shift,
                        0,sin(turningangle),cos(turningangle),z_shift,
                        0,0,0,1;


  //        Mat Positionmatrix(3,4);
  // Positionmatrix.at<float>(0,0)=cos(turningangle);
  // Positionmatrix.at<float>(0,1)=0;
  // Positionmatrix.at<float>(0,2)=sin(turningangle);
  // Positionmatrix.at<float>(0,3)=x_shift;
  // Positionmatrix.at<float>(1,0)=0;    
  // Positionmatrix.at<float>(1,1)=1;
  // Positionmatrix.at<float>(1,2)=0;
  // Positionmatrix.at<float>(1,3)=y_shift;
  // Positionmatrix.at<float>(2,0)=-sin(turningangle);
  // Positionmatrix.at<float>(2,1)=0;
  // Positionmatrix.at<float>(2,2)=cos(turningangle);
  // Positionmatrix.at<float>(2,3)=z_shift;

        
    }
























    void callback(const sensor_msgs::ImageConstPtr& image,const sensor_msgs::ImageConstPtr& image_depth,const keyboard::Key::ConstPtr& value)
    {
    if (value->code==49){

        //do something
        std::cout << "Background image will be taken:" << std::endl;
        cv_bridge::CvImagePtr cvimg = cv_bridge::toCvCopy(image);
        cv::Mat img_conv;
        // cv::cvtColor(cvimg->image,img_conv,COLOR_GRAY2BGR);
        cvimg->image.convertTo(img_conv,CV_8UC1,0.03);
        src1=img_conv;

        // imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/BackgroundImage.png/",img_conv);

        namedWindow("Background Image",CV_WINDOW_AUTOSIZE);
        imshow("Background Image",img_conv);


        imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/BackgroundImage.png",img_conv);


        waitKey(0)==113;
        destroyWindow("Background Image");

    }

    if (value->code==50){

        //do something
        std::cout << "Foreground image will be taken:" << std::endl;
        cv_bridge::CvImagePtr cvimg = cv_bridge::toCvCopy(image);
        cv_bridge::CvImagePtr cvimg_depth = cv_bridge::toCvCopy(image_depth);
        cv::Mat img_conv;
        cv::Mat img_conv_depth;
        // cv::cvtColor(cvimg->image,img_conv,COLOR_GRAY2BGR);
        cvimg->image.convertTo(img_conv,CV_8UC1,0.03);
        cvimg_depth->image.convertTo(img_conv_depth,CV_16U);
        src2=img_conv;
        depth=img_conv_depth;

        Mat imgsave;
        cvtColor(img_conv,imgsave,COLOR_GRAY2BGR);

        imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/ForegroundImage.png",img_conv);
        imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/DepthImage.png",img_conv_depth);



        namedWindow("Foreground Image",CV_WINDOW_AUTOSIZE);
        namedWindow("Depth Image",CV_WINDOW_AUTOSIZE);
        imshow("Foreground Image",img_conv);
        imshow("Depth Image",img_conv_depth);

        waitKey(0)==113;
        destroyWindow("Foreground Image");
        destroyWindow("Depth Image");


    }

    if (value->code==51){


       //do something
        std::cout << "Image Analysis will be performed:" << std::endl;
        GaussianBlur(src1,src1,Size(3,3),0,0);
        GaussianBlur(src2,src2,Size(3,3),0,0);
        Mat src3, src4;
        absdiff(src1,src2,src3);

        imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/DifferenceImage.png",src3);

        namedWindow("Difference Image",CV_WINDOW_AUTOSIZE);
        imshow("Difference Image", src3);
        
        GaussianBlur(src3,src3,Size(3,3),0,0);

//  imshow("difference denoised",src3);

    Mat blur, norm;
    normalize(src3,norm, 0, 255, NORM_MINMAX, CV_8UC1);
//    GaussianBlur(norm,blur,Size(15,15),0,0);
//    imshow("no gaussian",src33);
//      imshow("gaussian",blur);

    // imshow("normalized",norm);

    cvtColor(src3,src3,COLOR_GRAY2BGR);



    threshold(norm,src4,0,255,THRESH_BINARY | CV_THRESH_OTSU);

//    adaptiveThreshold(norm, src4, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 21,0);

    // imshow("otsu",src4);

      GaussianBlur(src4,src4,Size(3,3),0,0);

    imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/OtsuImage.png",src4);

    namedWindow("Otsu Image",CV_WINDOW_AUTOSIZE);
  
    imshow("Otsu Image",src4);


    // Perform the distance transform algorithm
    Mat dist;
    distanceTransform(src4, dist, CV_DIST_L2, 3);
    // Normalize the distance image for range = {0.0, 1.0}
    // so we can visualize and threshold it
    normalize(dist, dist, 0, 1., NORM_MINMAX);
    // imshow("Distance Transform Image", dist);
    // Threshold to obtain the peaks
    // This will be the markers for the foreground objects
    //threshold(dist, dist, .1, 1., CV_THRESH_BINARY);


    // Dilate a bit the dist image
    Mat kernel1 = Mat::ones(3, 3, CV_8UC1);
    dilate(dist, dist, kernel1);


    imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/DistanceTransform.png",dist);

    namedWindow("Distance Transform",CV_WINDOW_AUTOSIZE);
  
    imshow("Distance Transform",dist);
    // imshow("Peaks", dist);
    // Create the CV_8U version of the distance image
    // It is needed for findContours()
    Mat dist_8u;
    src4.convertTo(dist_8u, CV_8U);





    // Find total markers
    vector<vector<Point> > contours;
    findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    // Create the marker image for the watershed algorithm
    Mat markers = Mat::zeros(dist.size(), CV_32SC1);
    // Draw the foreground markers
    for (size_t i = 0; i < contours.size(); i++)
        drawContours(markers, contours, static_cast<int>(i), Scalar::all(static_cast<int>(i)+1), -1);
    // Draw the background marker
    circle(markers, Point(5,5), 3, CV_RGB(255,255,255), -1);
    // imshow("Markers", markers*10000);
    // Perform the watershed algorithm


    cvtColor(src4,src4,COLOR_GRAY2BGR);




    watershed(src4, markers);
    Mat mark = Mat::zeros(markers.size(), CV_8UC1);
    markers.convertTo(mark, CV_8UC1);
    bitwise_not(mark, mark);
    // imshow("Markers_v2", mark); // uncomment this if you want to see how the mark
                                  // image looks like at that point
    // Generate random colors
    vector<Vec3b> colors;
    for (size_t i = 0; i < contours.size(); i++)
    {
        int b = theRNG().uniform(0, 255);
        int g = theRNG().uniform(0, 255);
        int r = theRNG().uniform(0, 255);
        colors.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }
    // Create the result image
    Mat dst = Mat::zeros(markers.size(), CV_8UC3);
    // Fill labeled objects with random colors
    for (int i = 0; i < markers.rows; i++)
    {
        for (int j = 0; j < markers.cols; j++)
        {
            int index = markers.at<int>(i,j);
            if (index > 0 && index <= static_cast<int>(contours.size()))
                dst.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else
                dst.at<Vec3b>(i,j) = Vec3b(0,0,0);
        }
    }
    // Visualize the final image

    cvtColor(mark,mark,COLOR_GRAY2BGR);


    GaussianBlur(mark,dst,Size(21,21),0,0);
    GaussianBlur(dist,dist,Size(21,21),0,0);

    imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/FinalResult.png",dst);

    namedWindow("Final Result",CV_WINDOW_AUTOSIZE);

    imshow("Final Result", dst);






    cvtColor(dst,dst,COLOR_BGR2GRAY);
    cvtColor(src4,src4,COLOR_BGR2GRAY);




    int thresh=200;

    Mat srccorner, graycorner;
    // Load source image and convert it to gray
    srccorner = dist;
//    cvtColor( srccorner, graycorner, CV_BGR2GRAY );
    Mat dst_corner, dst_norm_corner, dst_norm_scaled_corner;
    dst_corner = Mat::zeros( srccorner.size(), CV_32FC1 );

    // Detecting corners
    cornerHarris( srccorner, dst_corner, 7, 5, 0.05, BORDER_DEFAULT );

    // Normalizing
    normalize( dst_corner, dst_norm_corner, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
    convertScaleAbs( dst_norm_corner, dst_norm_scaled_corner );





    Mat cornerdet,edgesimg;
    cornerdet = Mat::zeros( srccorner.size(), CV_32FC1 );
    edgesimg= Mat::zeros( srccorner.size(), CV_32FC1 );

    // Drawing a circle around corners
    int maxrow=0;
    int mincol=0;
    float distanceleft=dst_norm_scaled_corner.rows;
    float distancelefttry;
    float distanceright=dst_norm_scaled_corner.cols;
    float distancerighttry;
    int rightcol;
    int rightrow;
    for( int j = 0; j < dst_norm_corner.rows ; j++ )
    { for( int i = 0; i < dst_norm_corner.cols; i++ )
    {
        if( (int) dst_norm_corner.at<float>(j,i) > thresh )
        {
            circle( cornerdet, Point( i, j), 10,  Scalar(150), 2, 8, 0 );
             distancelefttry=sqrt(pow((dst_norm_corner.rows-j),2)+pow(i,2));
             distancerighttry=sqrt(pow(dst_norm_corner.rows-j,2)+pow((dst_norm_corner.cols-i),2));

            if (distancelefttry<distanceleft){
                maxrow=j;
                mincol=i;
                distanceleft=distancelefttry;
            }
            if (distancerighttry<distanceright){
               rightrow=j;
               rightcol=i;
               distanceright=distancerighttry;
            }

        }
    }
    }



    circle( dst_norm_scaled_corner, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( rightcol, rightrow ), 50,  Scalar(255), 2, 8, 0 );



    imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/CornerCadidates.png",cornerdet);
    imwrite("/home/dgoglio/catkin_ws/src/comicbot_v2/Background_Image/CornerResults.png",edgesimg);


    // Showing the result
//    namedWindow( "corners_window", CV_WINDOW_AUTOSIZE );
    // imshow( "corners_window", dst_norm_scaled_corner);
    namedWindow("Corner Candidates",CV_WINDOW_AUTOSIZE);
    namedWindow("Corner Result",CV_WINDOW_AUTOSIZE);

    imshow("Corner Candidates",cornerdet);
    imshow("Corner Result",edgesimg);

    depthvalue=static_cast<float>(depth.at<unsigned short>(maxrow,mincol));
    depthvalue=depthvalue/1000;

    depthvalue_right=static_cast<float>(depth.at<unsigned short>(rightrow,rightcol));
    depthvalue_right=depthvalue_right/1000;


    camerapositions(0)=mincol;
    camerapositions(1)=maxrow;
    camerapositions(2)=1;

    camerapositions_right(0)=rightcol;
    camerapositions_right(1)=rightrow;
    camerapositions_right(2)=1;
    // K=K.inverse();
    camerapositions_transformed=depthvalue*K.inverse()*camerapositions;
    test=K.inverse();

    camerapositions_right_transformed=depthvalue_right*K.inverse()*camerapositions_right;

    // camerapositions_transformed=depthvalue*camerapositions;

    camerapositions_homogenious(0)=camerapositions_transformed(0);
    camerapositions_homogenious(1)=camerapositions_transformed(1);
    camerapositions_homogenious(2)=camerapositions_transformed(2);
    camerapositions_homogenious(3)=1;

    camerapositions_right_homogenious(0)=camerapositions_right_transformed(0);
    camerapositions_right_homogenious(1)=camerapositions_right_transformed(1);
    camerapositions_right_homogenious(2)=camerapositions_right_transformed(2);
    camerapositions_right_homogenious(3)=1;


    cornerworldframe=Shiftmatrix*camerapositions_homogenious;

    cornerworldframe_right=Shiftmatrix*camerapositions_right_homogenious;



    baseline=0.055;

    testterm=(cornerworldframe_right(0)-cornerworldframe(0))/abs(cornerworldframe(1)-cornerworldframe_right(1));
    comicorientation=atan2((cornerworldframe_right(0)-cornerworldframe(0)),abs(cornerworldframe(1)-cornerworldframe_right(1)));
    comicwidth=sqrt(pow((camerapositions_right_homogenious(0)-camerapositions_homogenious(0)),2)+pow((camerapositions_right_homogenious(1)-camerapositions_homogenious(1)),2));
    



    wrist_yaw_orientation=(baseline_rotation/180)*3.14159+comicorientation;



    leftspot_x=-cos(comicorientation)*comicmargin+sin(comicorientation)*(comicwidth/4-baseline/2);
    leftspot_y=-sin(comicorientation)*comicmargin-cos(comicorientation)*(comicwidth/4-baseline/2);

    handcorrection_x=cos(wrist_yaw_orientation)*0.095+sin(wrist_yaw_orientation)*0.005;
    handcorrection_y=sin(wrist_yaw_orientation)*0.095-cos(wrist_yaw_orientation)*0.005;

    handcorrection_x=0.095-handcorrection_x;
    handcorrection_y=-0.005-handcorrection_y;

    leftspot_x=leftspot_x+handcorrection_x;
    leftspot_y=leftspot_y+handcorrection_y;

    quaternion_x=0;
    quaternion_y=0;
    quaternion_z=sin(wrist_yaw_orientation/2);
    quaternion_w=cos(wrist_yaw_orientation/2);




    std_msgs::Float64MultiArray cornerpositions;
    cornerpositions.data.clear();
    cornerpositions.data.resize(27);

    // cornerpositions.data=cornerworldframe;
    
    cornerpositions.data[0]=cornerworldframe(0);
    cornerpositions.data[1]=cornerworldframe(1);
    cornerpositions.data[2]=cornerworldframe(2);
    cornerpositions.data[3]=cornerworldframe_right(0);
    cornerpositions.data[4]=cornerworldframe_right(1);
    cornerpositions.data[5]=cornerworldframe_right(2);
    cornerpositions.data[6]=camerapositions_transformed(0);
    cornerpositions.data[7]=camerapositions_transformed(1);
    cornerpositions.data[8]=camerapositions_transformed(2);
    cornerpositions.data[9]=depthvalue;
    cornerpositions.data[10]=depthvalue_right;
    cornerpositions.data[11]=comicorientation;
    cornerpositions.data[12]=comicwidth;
    cornerpositions.data[13]=leftspot_x;
    cornerpositions.data[14]=leftspot_y;
    cornerpositions.data[15]=wrist_yaw_orientation;
    cornerpositions.data[16]=quaternion_z;
    cornerpositions.data[17]=quaternion_w;
    cornerpositions.data[18]=mincol;
    cornerpositions.data[19]=maxrow;
    cornerpositions.data[20]=sin(turningangle);
    cornerpositions.data[21]=turningangle;
    cornerpositions.data[22]=Shiftmatrix(0,1);
    cornerpositions.data[23]=comicorientation;
    cornerpositions.data[24]=testterm;
    cornerpositions.data[25]=handcorrection_x;
    cornerpositions.data[26]=handcorrection_y;

    positionpub.publish(cornerpositions);


        waitKey(0)==113;
        destroyWindow("Difference Image");
        destroyWindow("Otsu Image");
        destroyWindow("Final Result");
        destroyWindow("Corner Candidates");
        destroyWindow("Corner Result");


    }


    }
private:
    Mat src1,src2,depth;
    double depthvalue;
    double depthvalue_right;

  ros::NodeHandle nh;
  ros::Publisher positionpub;
  message_filters::Subscriber<sensor_msgs::Image> image_sub;
  message_filters::Subscriber<sensor_msgs::Image> depth_sub;
  message_filters::Subscriber<keyboard::Key> key_sub;
  
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image,sensor_msgs::Image, keyboard::Key> MySyncPolicy;
  // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
  message_filters::Synchronizer<MySyncPolicy> sync;

  //camera intrinsics
  float alpha_x;
  float alpha_y;
  float u_0;
  float v_0;
  float gamma;
  Eigen::Matrix3f K;
  
  Eigen::Matrix3f test;
  float turningangle;
  float x_shift;
  float y_shift;
  float z_shift;

  float comicorientation;
  float comicwidth;
  float leftspot_x;
  float leftspot_y;
  float comicmargin;


  float quaternion_x;
  float quaternion_y;
  float quaternion_z;
  float quaternion_w;

  float baseline_rotation;

  float baseline;

  float wrist_yaw_orientation;

  float handcorrection_x;
  float handcorrection_y;

  float testterm;

  Eigen::Matrix4f Shiftmatrix;
  Eigen::Vector3f camerapositions;
  Eigen::Vector3f camerapositions_transformed;
  Eigen::Vector4f camerapositions_homogenious;

  Eigen::Vector3f camerapositions_right;
  Eigen::Vector3f camerapositions_right_transformed;
  Eigen::Vector4f camerapositions_right_homogenious;

  Eigen::Vector4f cornerworldframe;
  Eigen::Vector4f cornerworldframe_right;
 
};




int main(int argc, char **argv)
{
    ros::init(argc, argv, "vision_node");

    SubscribeAndPublish SAPObject;

    

  // cv::namedWindow("view");
  // cv::namedWindow("Final Result");
  // cv::namedWindow("corners_window");
  // cv::namedWindow("cornerdet");
  // cv::namedWindow("corners");

  //cv::startWindowThread();

   ros::spin();
  return 0;
}
