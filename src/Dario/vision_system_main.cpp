#include <opencv2/opencv.hpp>

//#include "imgcodecs.hpp"
//#include "highgui.hpp"
//#include "imgproc.hpp"

#include <iostream>

// include the librealsense C++ header file
#include <librealsense/rs.hpp>


using namespace cv;
using namespace std;


int main()
{

    // Create a context object. This object owns the handles to all connected realsense devices
    rs::context ctx;

    // Access the first available RealSense device
    rs::device * dev = ctx.get_device(0);
    //printf("\nUsing device 0, an %s\n", dev->get_name());
    //printf("    Serial number: %s\n", dev->get_serial());
    //printf("    Firmware version: %s\n", dev->get_firmware_version());

    // Configure Infrared stream to run at VGA resolution at 30 frames per second
    dev->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 30);
    //dev->enable_stream(rs::stream::depth, 640, 480, rs::format::z16, 30);
    //dev->enable_stream(rs::stream::infrared, 640, 480, rs::format::y8, 60);

    // Start streaming
    dev->start();

    // Camera warmup - Dropped several first frames to let auto-exposure stabilize
    for(int i = 0; i < 30; i++)
       dev->wait_for_frames();

    // Creating OpenCV Matrix from a color image
    Mat color(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);

    // Display in a GUI
    namedWindow("Display Image", WINDOW_AUTOSIZE );
    imshow("Display Image", color);

    Mat src = color;

    waitKey(0);




  // Creating OpenCV Matrix from a color image
std::cout << __LINE__ << __FILE__ <<std::endl;
   for(int i = 0; i < 30; i++)
       dev->wait_for_frames();
    Mat color2(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Display in a GUI
    namedWindow("Display Image", WINDOW_AUTOSIZE );
    imshow("Display Image", color2);

    Mat src2 = color2;
    
std::cout << __LINE__ << __FILE__ <<std::endl;
    waitKey(0);

	
/*
    string filename = "Background.jpg";
    if (filename.empty())
    {
        help();
        cout << "no image_name provided" << endl;
        return -1;
    }
    Mat src = imread("Background.jpg", -1);
    if(src.empty())
    {
        help();
        cout << "can not open " << filename << endl;
        return -1;
    }
    Mat src2 = imread("Foreground.jpg", -1);
    if(src2.empty())
    {
        help();
        cout << "can not open " << filename << endl;
        return -1;
    }
*/
std::cout << __LINE__ << __FILE__ <<std::endl;

    Mat src3,src4,norm2;


std::cout << __LINE__ << __FILE__ <<std::endl;
    absdiff(src,src2,src3);
    //absdiff(src3,src,src2);
std::cout << __LINE__ << __FILE__ <<std::endl;
    imshow("difference",src3);
    normalize(src3,norm2, 0, 255, NORM_MINMAX);
std::cout << __LINE__ << __FILE__ <<std::endl;
    cvtColor(src3,src3,CV_RGB2GRAY);
std::cout << __LINE__ << __FILE__ <<std::endl;
    equalizeHist(src3,norm2);
std::cout << __LINE__ << __FILE__ <<std::endl;
//    imshow("normalized",norm2);


    Mat src1, src22, src3_grey, src33;
    src1 = color;
    src22 = color2;
    //src1 = imread("Background.jpg", CV_8UC1);
    //src22 = imread("Foreground.jpg", CV_8UC1);
std::cout << __LINE__ << __FILE__ <<std::endl;
    absdiff(src1,src22,src33);
    //cvtColor(src33,src3_grey,CV_RGB2GRAY);


    Mat blur, norm;
std::cout << __LINE__ << __FILE__ <<std::endl;
    normalize(src33,norm, 0, 255, NORM_MINMAX, CV_8UC3);
std::cout << __LINE__ << __FILE__ <<std::endl;
    GaussianBlur(norm,blur,Size(15,15),0,0);
//    imshow("no gaussian",src33);
//    imshow("gaussian",blur);

Mat blur_gray;
cvtColor( blur, blur_gray, CV_BGR2GRAY );
std::cout << __LINE__ << __FILE__ <<std::endl;
    cv::threshold(blur_gray,src4,0,255,THRESH_BINARY | CV_THRESH_OTSU);

std::cout << __LINE__ << __FILE__ <<std::endl;
    //src3=src2;
    // Show source image

std::cout << __LINE__ << __FILE__ <<std::endl;
    Mat src5;
    cvtColor(src4, src5, COLOR_GRAY2BGR);
    src3=src2;

std::cout << __LINE__ << __FILE__ <<std::endl;
    imshow("Source Image", src3);
    // Change the background from white to black, since that will help later to extract
    // better results during the use of Distance Transform
    for( int x = 0; x < src3.rows; x++ ) {
      for( int y = 0; y < src3.cols; y++ ) {
          if ( src3.at<Vec3b>(x, y) == Vec3b(255,255,255) ) {
            src3.at<Vec3b>(x, y)[0] = 0;
            src3.at<Vec3b>(x, y)[1] = 0;
            src3.at<Vec3b>(x, y)[2] = 0;
          }
        }
    }
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Show output image
    imshow("Black Background Image", src3);
    // Create a kernel that we will use for accuting/sharpening our image
    Mat kernel = (Mat_<float>(3,3) <<
            1,  1, 1,
            1, -8, 1,
            1,  1, 1); // an approximation of second derivative, a quite strong kernel
    // do the laplacian filtering as it is
    // well, we need to convert everything in something more deeper then CV_8U
    // because the kernel has some negative values,
    // and we can expect in general to have a Laplacian image with negative values
    // BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
    // so the possible negative number will be truncated
std::cout << __LINE__ << __FILE__ <<std::endl;
    Mat imgLaplacian;
    Mat sharp = src3; // copy source image to another temporary one
    filter2D(sharp, imgLaplacian, CV_32F, kernel);
    src3.convertTo(sharp, CV_32F);
    Mat imgResult = sharp - imgLaplacian;
std::cout << __LINE__ << __FILE__ <<std::endl;
    // convert back to 8bits gray scale
    imgResult.convertTo(imgResult, CV_8UC3);
    imgLaplacian.convertTo(imgLaplacian, CV_8UC3);
    // imshow( "Laplace Filtered Image", imgLaplacian );
//    imshow( "New Sharped Image", imgResult );
    src3 = imgResult; // copy back
    // Create binary image from source image
    Mat bw;
    cvtColor(src3, bw, CV_BGR2GRAY);
    threshold(bw, bw, 40, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
    imshow("Binary Image", bw);
    // Perform the distance transform algorithm
    Mat dist;
    distanceTransform(src4, dist, CV_DIST_L2, 3);
    // Normalize the distance image for range = {0.0, 1.0}
    // so we can visualize and threshold it
    normalize(dist, dist, 0, 1., NORM_MINMAX);
    imshow("Distance Transform Image", dist);
    // Threshold to obtain the peaks
    // This will be the markers for the foreground objects
    threshold(dist, dist, .4, 1., CV_THRESH_BINARY);
    // Dilate a bit the dist image
std::cout << __LINE__ << __FILE__ <<std::endl;
    Mat kernel1 = Mat::ones(3, 3, CV_8UC1);
std::cout << __LINE__ << __FILE__ <<std::endl;
    dilate(dist, dist, kernel1);
std::cout << __LINE__ << __FILE__ <<std::endl;
    imshow("Peaks", dist);
    // Create the CV_8U version of the distance image
    // It is needed for findContours()
    Mat dist_8u;
    dist.convertTo(dist_8u, CV_8U);
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Find total markers
    vector<vector<Point> > contours;
    findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Create the marker image for the watershed algorithm
    Mat markers = Mat::zeros(dist.size(), CV_32SC1);
    // Draw the foreground markers
    for (size_t i = 0; i < contours.size(); i++)
        drawContours(markers, contours, static_cast<int>(i), Scalar::all(static_cast<int>(i)+1), -1);
    // Draw the background marker
    circle(markers, Point(5,5), 3, CV_RGB(255,255,255), -1);
std::cout << __LINE__ << __FILE__ <<std::endl;
    imshow("Markers", markers*10000);
    // Perform the watershed algorithm
    watershed(src3, markers);
std::cout << __LINE__ << __FILE__ <<std::endl;
    Mat mark = Mat::zeros(markers.size(), CV_8UC3);
    markers.convertTo(mark, CV_8UC3);
    bitwise_not(mark, mark);
    imshow("Markers_v2", mark); // uncomment this if you want to see how the mark
                                  // image looks like at that point
    // Generate random colors
std::cout << __LINE__ << __FILE__ <<std::endl;
    vector<Vec3b> colors;
    for (size_t i = 0; i < contours.size(); i++)
    {
        int b = theRNG().uniform(0, 255);
        int g = theRNG().uniform(0, 255);
        int r = theRNG().uniform(0, 255);
        colors.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }
    // Create the result image
    Mat dst = Mat::zeros(markers.size(), CV_8UC3);
    // Fill labeled objects with random colors
    for (int i = 0; i < markers.rows; i++)
    {
        for (int j = 0; j < markers.cols; j++)
        {
            int index = markers.at<int>(i,j);
            if (index > 0 && index <= static_cast<int>(contours.size()))
                dst.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else
                dst.at<Vec3b>(i,j) = Vec3b(0,0,0);
        }
    }
std::cout << __LINE__ << __FILE__ <<std::endl;
    // Visualize the final image



    GaussianBlur(dst,dst,Size(41,41),0,0);


    imshow("Final Result", dst);





    cvtColor(dst,dst,COLOR_BGR2GRAY);

    Mat dst2, cdst2;
    Canny(dst, dst2, 100, 200, 3);
    cvtColor(dst2, cdst2, COLOR_GRAY2BGR);
    imshow("canny",dst2);

//#if 0
    vector<Vec2f> lines;
    HoughLines(dst2, lines, 1, CV_PI/180, 100, 0, 0 );

    for( size_t i = 0; i < lines.size(); i++ )
    {
        float rho = lines[i][0], theta = lines[i][1];
        Point pt1, pt2;
        double a = cos(theta), b = sin(theta);
        double x0 = a*rho, y0 = b*rho;
        pt1.x = cvRound(x0 + 1000*(-b));
        pt1.y = cvRound(y0 + 1000*(a));
        pt2.x = cvRound(x0 - 1000*(-b));
        pt2.y = cvRound(y0 - 1000*(a));
        line( cdst2, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
    }
//#else
//    vector<Vec4i> lines;
//    HoughLinesP(dst2, lines, 1, CV_PI/180, 50, 50, 10 );
//    for( size_t i = 0; i < lines.size(); i++ )
//    {
//        Vec4i l = lines[i];
//        line( cdst2, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, LINE_AA);
//    }
//#endif
    imshow("detected lines", cdst2);

    int thresh=200;

    Mat srccorner, graycorner;
    // Load source image and convert it to gray
    srccorner = dst;
//    cvtColor( srccorner, graycorner, CV_BGR2GRAY );
    Mat dst_corner, dst_norm_corner, dst_norm_scaled_corner;
    dst_corner = Mat::zeros( srccorner.size(), CV_32FC1 );

    // Detecting corners
    cornerHarris( srccorner, dst_corner, 7, 5, 0.05, BORDER_DEFAULT );

    // Normalizing
    normalize( dst_corner, dst_norm_corner, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
    convertScaleAbs( dst_norm_corner, dst_norm_scaled_corner );





    Mat cornerdet,edgesimg;
    cornerdet = Mat::zeros( srccorner.size(), CV_32FC1 );
    edgesimg= Mat::zeros( srccorner.size(), CV_32FC1 );

    // Drawing a circle around corners
    int maxrow=0;
    int mincol=0;
    float distanceleft=dst_norm_scaled_corner.rows;
    float distancelefttry;
    float distanceright=dst_norm_scaled_corner.cols;
    float distancerighttry;
    int rightcol;
    int rightrow;
    for( int j = 0; j < dst_norm_corner.rows ; j++ )
    { for( int i = 0; i < dst_norm_corner.cols; i++ )
    {
        if( (int) dst_norm_corner.at<float>(j,i) > thresh )
        {
            circle( cornerdet, Point( i, j), 10,  Scalar(150), 2, 8, 0 );
             distancelefttry=sqrt(pow((dst_norm_corner.rows-j),2)+pow(i,2));
             distancerighttry=sqrt(pow(dst_norm_corner.rows-j,2)+pow((dst_norm_corner.cols-i),2));

            if (distancelefttry<distanceleft){
                maxrow=j;
                mincol=i;
                distanceleft=distancelefttry;
            }
            if (distancerighttry<distanceright){
               rightrow=j;
               rightcol=i;
               distanceright=distancerighttry;
            }

        }
    }
    }



    circle( dst_norm_scaled_corner, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( mincol, maxrow ), 50,  Scalar(255), 2, 8, 0 );
    circle( edgesimg, Point( rightcol, rightrow ), 50,  Scalar(255), 2, 8, 0 );



    // Showing the result
//    namedWindow( "corners_window", CV_WINDOW_AUTOSIZE );
    imshow( "corners_window", dst_norm_scaled_corner);
    imshow("cornerdet",cornerdet);
    imshow("corners",edgesimg);

//    Mat cdst3;

//    cvtColor(dst_norm_scaled_corner, cdst3, COLOR_GRAY2BGR);


//    vector<Vec2f> lines2;
//    HoughLines(dst_norm_scaled_corner, lines2, 1, CV_PI/180, 100, 0, 0 );

//    for( size_t i = 0; i < lines2.size(); i++ )
//    {
//        float rho = lines2[i][0], theta = lines2[i][1];
//        Point pt1, pt2;
//        double a = cos(theta), b = sin(theta);
//        double x0 = a*rho, y0 = b*rho;
//        pt1.x = cvRound(x0 + 1000*(-b));
//        pt1.y = cvRound(y0 + 1000*(a));
//        pt2.x = cvRound(x0 - 1000*(-b));
//        pt2.y = cvRound(y0 - 1000*(a));
//        line( cdst3, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
//    }




//imshow("new hough",cdst3);

//Mat dst_gray;
//int thresh = 200;
//int max_thresh = 255;
//const char* corners_window = "Corners detected";



//cvtColor(dst,dst_gray,COLOR_BGR2GRAY);





//    Mat dst2, dst_norm, dst_norm_scaled;
//    dst2 = Mat::zeros( dst.size(), CV_32FC1 );

//    /// Detector parameters
//    int blockSize = 2;
//    int apertureSize = 3;
//    double k = 0.04;

//    /// Detecting corners
//    cornerHarris( dst_gray, dst2, blockSize, apertureSize, k, BORDER_DEFAULT );

//    imshow("corners",dst2);
//    /// Normalizing
//    normalize( dst2, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
//    convertScaleAbs( dst_norm, dst_norm_scaled );


//    /// Drawing a circle around corners
//    for( int j = 0; j < dst_norm.rows ; j++ )
//       { for( int i = 0; i < dst_norm.cols; i++ )
//            {
//              if( (int) dst_norm.at<float>(j,i) > thresh )
//                {
//                 circle( dst_norm_scaled, Point( i, j ), 5,  Scalar(0), 2, 8, 0 );
//                }
//            }
//       }
//    /// Showing the result
//    namedWindow( corners_window, WINDOW_AUTOSIZE );
//    imshow( corners_window, dst_norm_scaled );







    //threshold(src3,src3,20,255, CV_THRESH_BINARY);
    //imshow("thresh",src3);
    //Mat dst, cdst;
    //Canny(src3, dst, 50, 200, 3);
    //cvtColor(dst, cdst, COLOR_GRAY2BGR);
    //Mat cdst;

//#if 0
//    vector<Vec2f> lines;
//    HoughLines(dst, lines, 1, CV_PI/180, 100, 0, 0 );
//
//    for( size_t i = 0; i < lines.size(); i++ )
//   {
//        float rho = lines[i][0], theta = lines[i][1];
//        Point pt1, pt2;
//        double a = cos(theta), b = sin(theta);
//        double x0 = a*rho, y0 = b*rho;
//        pt1.x = cvRound(x0 + 1000*(-b));
//        pt1.y = cvRound(y0 + 1000*(a));
//        pt2.x = cvRound(x0 - 1000*(-b));
//        pt2.y = cvRound(y0 - 1000*(a));
//       line( cdst, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
//    }
//#else
//    vector<Vec4i> lines;
//    HoughLinesP(dst, lines, 1, CV_PI/180, 50, 50, 10 );
 //   for( size_t i = 0; i < lines.size(); i++ )
 //   {
 //       Vec4i l = lines[i];
//        line( cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, LINE_AA);
//    }
//#endif
//    imshow("source", src);
// //   imshow("detected lines", cdst);

    waitKey();

    return 0;
}
