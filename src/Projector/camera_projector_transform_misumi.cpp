/*
 * main.cpp
 *
 *  Created on: Aug 2, 2017
 *      Author: iveta
 */

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

using namespace cv;
using namespace std;

const float COMPRESSION_CONST =0.25;

const int width_proj  = 1280; //projector resolution
const int height_proj = 720;

const int width_first  = 1680; //monitor resolution
const int height_first = 1050;

main(){

//create the checkerboard
	Mat pattern_data(Size( width_proj,height_proj), CV_8UC3, Scalar(255,255,255));
	vector<Point2f> proj_points;

	int quad_size=85;
	int chess_board_width = 12;
	int chess_board_height = 7;
	int x_offset = 0;
	int y_offset = 0;
	Vec3b black=(0,0,0);

	int w_start = (width_proj-(chess_board_width+1)*quad_size)/2 + x_offset;
	int h_start = (height_proj-(chess_board_height+1)*quad_size)/2 + y_offset;
	int min_x = w_start + quad_size;
	int min_y = h_start + quad_size;
	int max_x = w_start + chess_board_width * quad_size;
	int max_y = h_start + chess_board_height * quad_size;

	for (int i = 0; i < chess_board_height+1; i++)
	{
		for (int j = 0; j < chess_board_width+1; j++)
		{
			int x = (w_start + j * quad_size);
			int y = (h_start + i * quad_size);
			// store 2D reference points
			if (i > 0 && j > 0)
			{
				proj_points.push_back(Point2f((float)x, (float)y));
			}
			// save checkerboard
			if ((i + j) % 2)
			{
				for (int ii = x; ii < x+quad_size; ++ii)
				{
					for (int jj = y; jj < y+quad_size; ++jj)
					{
						if (ii < (int)width_proj && jj < (int)height_proj)
						{
							pattern_data.at<Vec3b>(jj,ii) = black;
						}
					}
				}
			}
		}
	}


//display the checkerboard

	namedWindow("chessboard",CV_WINDOW_NORMAL);
	moveWindow("chessboard", width_first, 0);
	setWindowProperty("chessboard", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	imshow("chessboard",pattern_data);
	waitKey(0);
	
	
//set up the camera and take a picture
	
	VideoCapture cap(0); // open the default camera
		if(!cap.isOpened()){// check if we succeeded
		
			printf("Camera not found");
			return -1;
		}
		cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
		cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

		cap.set(CV_CAP_PROP_FPS,10);
		cap.set(CV_CAP_PROP_FRAME_WIDTH,1920);
		cap.set(CV_CAP_PROP_FRAME_HEIGHT,1080);
		
		cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
		cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

		Mat cam_image;
		cap >> cam_image; // get a new frame from cameraK
		
		
		resize(cam_image, cam_image, Size(), COMPRESSION_CONST, COMPRESSION_CONST, CV_INTER_AREA);
		

	namedWindow("image",CV_WINDOW_AUTOSIZE);
	imshow("image",cam_image);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers.png",cam_image);
	waitKey(0);
	destroyWindow("image");
	destroyWindow("chessboard");

	
	cout<<" proj_points "<<endl<<" "<<proj_points<< endl << endl;
	drawChessboardCorners(pattern_data, Size(chess_board_width,chess_board_height), proj_points,true);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers_drawn1.png",pattern_data);
	
//find checkers on the camera image
	vector<Point2f> cam_points;
	bool found=findChessboardCorners(cam_image, Size(chess_board_width,chess_board_height), cam_points,
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE);

	cout<<" cam_points "<<endl<<" "<<cam_points<< endl<< endl;
	
	
	vector<Point2f> cam_points_help(cam_points.size());
	for(int i=cam_points.size(); i>0;i--){
		cam_points_help[cam_points.size()-i]=cam_points[i-1]; 
	}
	cam_points=cam_points_help;
	cout<<" cam_points "<<endl<<" "<<cam_points<< endl<< endl;
	
	
	drawChessboardCorners(cam_image, Size(chess_board_width,chess_board_height), cam_points,found);
	namedWindow("image",CV_WINDOW_AUTOSIZE);
	imshow("image",cam_image);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers_drawn2.png",cam_image);
	waitKey(0);
	destroyWindow("image");
	
//calculate homography


	Mat homo_cam_to_proj = findHomography(cam_points, proj_points);
	cout<<" homo_cam_to_proj "<<endl<<" "<<homo_cam_to_proj<< endl<< endl;
//test the result
	/*
	Mat cam_image_test(Size(640, 480), CV_8UC3, Scalar(0,0,0));
	for(int i=0;i<cam_points.size();i++){
		circle(cam_image_test,cam_points[i],20, Scalar(255,255,255),5);
	}
	namedWindow("image",CV_WINDOW_AUTOSIZE);
	imshow("image",cam_image_test);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers_test.png",cam_image_test);
	waitKey(0);
	destroyWindow("image");
	
	vector<Point2f> proj_points_test;
	//perspectiveTransform(cam_points, proj_points_test,  homo_cam_to_proj);
	
	std::vector<Point2f> cam_points_test(4);
	//cam_points_test[0] = Point2f(88,49);
	//cam_points_test[1] = Point2f(265,61);
	//cam_points_test[2] = Point2f(283.675,175.71278);
	//cam_points_test[3] = Point2f(28.503,143.090);
	cam_points_test[0] = Point2f(148,93);
	cam_points_test[1] = Point2f(588,119);
	cam_points_test[2] = Point2f(45.675,288.71278);
	cam_points_test[3] = Point2f(566.503,351.090);
	cout<<" cam_points_test "<<endl<<" "<<cam_points_test<< endl<< endl;
	perspectiveTransform(cam_points_test, proj_points_test,  homo_cam_to_proj);
	cout<<" cam_points_test "<<endl<<" "<<cam_points_test<< endl<< endl;
	Mat projection(Size(1280, 720), CV_8UC3, Scalar(0,0,0));
	for(int i=0;i<cam_points.size();i++){
			circle(projection,proj_points_test[i],20, Scalar(255,255,255),5);
		}
	//circle(projection,proj_points_test[0],20, Scalar(255,255,255),5);
	//circle(projection,proj_points_test[1],20, Scalar(255,255,255),5);
	//circle(projection,proj_points_test[2],20, Scalar(255,255,255),5);
	//circle(projection,proj_points_test[3],20, Scalar(255,255,255),5);
	
	namedWindow("image",CV_WINDOW_AUTOSIZE);
	imshow("image",projection);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers_test2.png",projection);
	waitKey(0);
	destroyWindow("image");
	
	namedWindow("chessboard",CV_WINDOW_NORMAL);
	moveWindow("chessboard", width_first, 0);
	setWindowProperty("chessboard", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	imshow("chessboard",projection);
	waitKey(0);
	
	*/
	
	return 0;
}
