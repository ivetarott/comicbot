/*
 * main.cpp
 *
 *  Created on: Aug 2, 2017
 *      Author: iveta
 */

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

using namespace cv;
using namespace std;

const int NUMBER_PAGES = 51;
const float MAXIMUM_MATCHES_DIFFERENCE = 0.7; //second best page pick can have maximum
											//70% of matches of the first pick

/*
//Draws the keypoint matches detected and saves
void show_matches(Mat color, std::vector<cv::KeyPoint> keypoints, int page_number,
		std::vector< DMatch > good_matches){

	std::ostringstream os;
	os << std::setw( 4 ) << std::setfill( '0' ) << page_number;
	string filename = "/home/iveta/realsense_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + "_compressed.jpg";
	Mat color2=imread(filename,1);

	Mat img_matches;
	drawMatches(color, keypoints, color2, keypoints2, good_matches, img_matches,
			 Scalar::all(-1), Scalar::all(-1),vector<char>(),
			 DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
	filename="/home/iveta/realsense_ws/src/comicbot/images/matches/good_matches_"+ os.str()+".png";
	imwrite(filename,img_matches);

}
*/

//compares the descriptors against a database
void recognize_page(Mat descriptors, int &best_page,vector<KeyPoint> &keypoints2,
		vector< DMatch > &good_matches_final){
	best_page=0;
	int most_matches=0;
	int second_most_matches=0;
	FileStorage fs("/home/iveta/realsense_ws/src/comicbot/descriptors/comics_descriptors.yml", FileStorage::READ);

	for(int i=1; i<= NUMBER_PAGES; i++){

		printf("Matching against page number %d \n", i);
		Mat descriptors2;

		string iValue= to_string(i);
		string filename = "descriptors_"+ iValue;
		fs[filename] >> descriptors2;

		//Matching descriptor vectors using FLANN matcher
		FlannBasedMatcher matcher;
		std::vector< DMatch > matches;
		matcher.match(descriptors, descriptors2, matches);

		//Calculation of max and min distances between keypoints
		double max_dist = 0; double min_dist = 100;
		for( int i = 0; i < descriptors.rows; i++ ){
			 double dist = matches[i].distance;
			 if( dist < min_dist ) min_dist = dist;
			 if( dist > max_dist ) max_dist = dist;
		}

		//Pick only "good" matches
		std::vector< DMatch > good_matches;
		for( int i = 0; i < descriptors.rows; i++ ){
			if( matches[i].distance <= max(2*min_dist, 0.02) ){
				 good_matches.push_back(matches[i]);
			}
		}

		/*
		//Prints number of matches on the currently compared page
		printf("%zd matches have been found with double-page %d. \n",
				good_matches.size(), i);
		*/

		//tracks the page with the most matches
		if (good_matches.size()> most_matches){
			most_matches= good_matches.size();
			best_page=i;
			good_matches_final=good_matches;
		}
		else if (good_matches.size()> second_most_matches){
			second_most_matches= good_matches.size();
		}

	} //end loop over all pages in the database

	//checking the confidence as opposed to second best choice of a page
	if ( ((float)second_most_matches/most_matches) < MAXIMUM_MATCHES_DIFFERENCE){
		//get the keypoints for further analysis
		string iValue = to_string(best_page);
		string filename = "keypoints_"+ iValue;
		FileNode kptNode = fs[filename];
		read(kptNode, keypoints2);
		fs.release(); //close the file with descriptors database
	}
	else
		best_page=0;

}


int main()
{
	//set up camera and start streaming//

	// Create a context object. This object owns the handles to all connected realsense devices
	rs::context ctx;

	// Access the first available RealSense device
	rs::device * dev = ctx.get_device(0);
	printf("\nUsing device 0, an %s\n", dev->get_name());
	printf("    Serial number: %s\n", dev->get_serial());
	printf("    Firmware version: %s\n", dev->get_firmware_version());

	// Configure color stream to run at VGA resolution at 30 frames per second
	dev->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 30);

	// Start streaming
	dev->start();

	// Camera warm-up - Dropped several first frames to let auto-exposure stabilize
	for(int i = 0; i < 30; i++){
		dev->wait_for_frames();
	}

	//take an image
	cout << "An image will be taken:" << endl;
	Mat color(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);
	cout << "An image has been taken." << endl;

	//show on screen and save the image taken
	namedWindow("image",CV_WINDOW_AUTOSIZE);
	imshow("image",color);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image.png",color);
	waitKey(0);
	destroyWindow("image");

	dev->stop();

	//compression for faster and more accurate analysis
	resize(color, color, Size(), 0.6, 0.6, CV_INTER_AREA);

	//Keypoint detection
	SiftFeatureDetector detector;
	vector<KeyPoint> keypoints;
	detector.detect(color, keypoints);

/*
	// Draw keypoints on the image taken, show and save.
	Mat output;
	drawKeypoints(color, keypoints, output);
	namedWindow("sift_keypoints1",CV_WINDOW_AUTOSIZE);
	imshow("sift_keypoints",output);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/sift_keypoints.jpg", output);
	waitKey(0);
*/

	//Feature extraction
	Mat descriptors;
	SiftDescriptorExtractor extractor;
	extractor.compute(color, keypoints, descriptors);
	printf("Image 1:%zd keypoints were found.\n", keypoints.size());

	//pick the right page from the comic database
	std::vector< DMatch > good_matches;
	vector<KeyPoint> keypoints2;

	//find the matching page in the comics
	int best_page;
	recognize_page(descriptors, best_page, keypoints2, good_matches);	 //returns the double-page number or 0 if not recognized
	if (best_page == 0) {
		printf("Not good enough matches to recognize the page! \n");
		printf("The program is closing. \n");
		return 0; //end of main
	}
	printf("This is a double-page number %d (pages %d - %d) \n",
					best_page, (best_page-1)*2, (best_page-1)*2+1);

/*
	//Show the detected comic page on screen:
	std::ostringstream os;
	os << std::setw( 4 ) << std::setfill( '0' ) << best_page;
	string filename = "/home/iveta/realsense_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + "_compressed.jpg";
	Mat result = imread(filename,1);
	namedWindow("detected_page",CV_WINDOW_AUTOSIZE);
	imshow("detected_page",result);
	waitKey();
*/

	//



	return 0;
}
