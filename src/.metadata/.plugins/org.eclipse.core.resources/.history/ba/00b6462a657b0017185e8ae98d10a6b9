/*
 * main.cpp
 *
 *  Created on: Aug 2, 2017
 *      Author: iveta
 */

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

using namespace cv;
using namespace std;

//constants for matching pages
const float COMPRESSION_CONST =0.6;
const int NUMBER_PAGES = 51;
const float MAXIMUM_MATCHES_DIFFERENCE = 0.7; //second best page pick can have maximum
											//70% of matches of the first pick

//camera intrinsic
const float ALPHA_X=597.176170739031;
const float ALPHA_Y=596.5321536080638;
const float U_0=327.7837240751012;
const float V_0=233.27450741379263;
const float CAM_GAMMA=0.0;
const vector<float> DIST_COEFF= {0.0795118332956917,-0.20023674829691393,-0.0011159545498659685,
		0.001468490794944076};

//camera extrinsic
const float TURNING_ANGLE=M_PI/4;
const float X_SHIFT=0.0;
const float Y_SHIFT=0.0;
const float Z_SHIFT=0.295;




//compares the descriptors against a database
void recognize_page(Mat descriptors, int &best_page,vector<KeyPoint> &keypoints2,
		vector< DMatch > &good_matches_final){
	best_page=0;
	int most_matches=0;
	int second_most_matches=0;
	FileStorage fs("/home/iveta/realsense_ws/src/comicbot/descriptors/comics_descriptors.yml", FileStorage::READ);

	for(int i=1; i<= NUMBER_PAGES; i++){

		printf("Matching against page number %d \n", i);
		Mat descriptors2;

		string iValue= to_string(i);
		string filename = "descriptors_"+ iValue;
		fs[filename] >> descriptors2;

		//Matching descriptor vectors using FLANN matcher
		FlannBasedMatcher matcher;
		std::vector< DMatch > matches;
		matcher.match(descriptors, descriptors2, matches);

		//Calculation of max and min distances between keypoints
		double max_dist = 0; double min_dist = 100;
		for( int i = 0; i < descriptors.rows; i++ ){
			 double dist = matches[i].distance;
			 if( dist < min_dist ) min_dist = dist;
			 if( dist > max_dist ) max_dist = dist;
		}

		//Pick only "good" matches
		std::vector< DMatch > good_matches;
		for( int i = 0; i < descriptors.rows; i++ ){
			if( matches[i].distance <= max(2*min_dist, 0.2) ){
				 good_matches.push_back(matches[i]);
			}
		}

		/*
		//Prints number of matches on the currently compared page
		printf("%zd matches have been found with double-page %d. \n",
				good_matches.size(), i);
		*/

		//tracks the page with the most matches
		if (good_matches.size()> most_matches){
			most_matches= good_matches.size();
			best_page=i;
			good_matches_final=good_matches;
		}
		else if (good_matches.size()> second_most_matches){
			second_most_matches= good_matches.size();
		}

	} //end loop over all pages in the database

	//checking the confidence as opposed to second best choice of a page
	if ( ((float)second_most_matches/most_matches) < MAXIMUM_MATCHES_DIFFERENCE){
		//get the keypoints for further analysis
		string iValue = to_string(best_page);
		string filename = "keypoints_"+ iValue;
		FileNode kptNode = fs[filename];
		read(kptNode, keypoints2);
		fs.release(); //close the file with descriptors database
	}
	else
		best_page=0;

}


void localize(vector<KeyPoint> keypoints, vector<KeyPoint> keypoints2,
		std::vector< DMatch > good_matches, Mat &img_matches, Mat color2,
		vector<Point2f> &comic_corners_image, vector<Point3f> &comic_corners_object){

	vector<Point2f> scene;
	vector<Point2f> book;
	//Get the keypoints from the good matches
	for( int i = 0; i < good_matches.size(); i++ ){
		scene.push_back(keypoints[good_matches[i].queryIdx].pt);
		book.push_back(keypoints2[good_matches[i].trainIdx].pt);
	}
	Mat H = findHomography(book, scene, CV_RANSAC);

	//Get the corners from the book
	std::vector<Point2f> object_corners(4);
	object_corners[0] = cvPoint(0,0);
	object_corners[1] = cvPoint(color2.cols,0);
	object_corners[2] = cvPoint(color2.cols,color2.rows);
	object_corners[3] = cvPoint(0,color2.rows);

	comic_corners_object= {{0f,0f,0f},{color2.cols,0f,0f},{color2.cols,color2.rows,0f},
			{0f,color2.rows,0f}};



	std::vector<Point2f> scene_corners(4);
	perspectiveTransform(object_corners, scene_corners, H);
	comic_corners_image = scene_corners;

	//Draw lines between the corners of the mapped object in the scene
	line(img_matches, scene_corners[0], scene_corners[1], Scalar(0,255,0),2);
	line(img_matches, scene_corners[1], scene_corners[2], Scalar(0,255,0),2);
	line(img_matches, scene_corners[2], scene_corners[3], Scalar(0,255,0),2);
	line(img_matches, scene_corners[3], scene_corners[0], Scalar(0,255,0),2);

	string filename="/home/iveta/realsense_ws/src/comicbot/images/result.png";
	imwrite(filename,img_matches);
}



int main()
{
	///////////////////SET UP CAMERA AND START STREAMING/////////////////////////

	// Create a context object. This object owns the handles to all connected realsense devices
	rs::context ctx;

	// Access the first available RealSense device
	rs::device * dev = ctx.get_device(0);
	printf("\nUsing device 0, an %s\n", dev->get_name());
	printf("    Serial number: %s\n", dev->get_serial());
	printf("    Firmware version: %s\n", dev->get_firmware_version());

	// Configure color stream to run at VGA resolution at 30 frames per second
	dev->enable_stream(rs::stream::color, 640, 480, rs::format::bgr8, 30);

	// Start streaming
	dev->start();

	// Camera warm-up - Dropped several first frames to let auto-exposure stabilize
	for(int i = 0; i < 30; i++){
		dev->wait_for_frames();
	}

	////////////////////////TAKE AN IMAGE//////////////////////
	cout << "An image will be taken:" << endl;
	Mat color(Size(640, 480), CV_8UC3, (void*)dev->get_frame_data(rs::stream::color), Mat::AUTO_STEP);
	cout << "An image has been taken." << endl;

	//show on screen and save the image taken
	namedWindow("image",CV_WINDOW_AUTOSIZE);
	imshow("image",color);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image.png",color);
	waitKey(0);
	destroyWindow("image");

	dev->stop();
	/////////////////////IMAGE ANALYSIS//////////////////////////
	//compression for faster and more accurate analysis
	resize(color, color, Size(), COMPRESSION_CONST, COMPRESSION_CONST, CV_INTER_AREA);

	//Keypoint detection
	SiftFeatureDetector detector;
	vector<KeyPoint> keypoints;
	detector.detect(color, keypoints);

	// Draw keypoints on the image taken, show and save.
	Mat output;
	drawKeypoints(color, keypoints, output);
	namedWindow("sift_keypoints1",CV_WINDOW_AUTOSIZE);
	imshow("sift_keypoints",output);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/sift_keypoints.jpg", output);
	waitKey(0);

	//Feature extraction
	Mat descriptors;
	SiftDescriptorExtractor extractor;
	extractor.compute(color, keypoints, descriptors);
	printf("Image 1:%zd keypoints were found.\n", keypoints.size());

	//find the right page from the comic database
	std::vector< DMatch > good_matches;
	vector<KeyPoint> keypoints2;
	int best_page;
	recognize_page(descriptors, best_page, keypoints2, good_matches);	 //returns the double-page number or 0 if not recognized
	if (best_page == 0) {
		printf("Not good enough matches to recognize the page! \n");
		printf("The program is closing. \n");
		return 0; //end of main
	}
	printf("This is a double-page number %d (pages %d - %d) \n",
					best_page, (best_page-1)*2, (best_page-1)*2+1);

	//Draw the keypoint matches
	Mat img_matches;
	std::ostringstream os;
	os << std::setw( 4 ) << std::setfill( '0' ) << best_page;
	string filename = "/home/iveta/realsense_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + "_compressed.jpg";
	Mat color2=imread(filename,1);
	drawMatches(color, keypoints, color2, keypoints2, good_matches, img_matches,
			 Scalar::all(-1), Scalar::all(-1),vector<char>(),
			 DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

	//Localize the comic book in the scene
	vector<Point2f> comic_corners_image(4);
	vector<Point3f> comic_corners_object(4);
	localize(keypoints, keypoints2, good_matches, img_matches, color2,
			comic_corners_image, comic_corners_object);
	//Show detected matches and the localized book
	imshow( "Good Matches and Object detection", img_matches );
	waitKey(0);

	////////////////////COMICBOOK POSE ESTIMATION////////////
	Mat K(3,3,CV_32F,Scalar::all(0));
	K.at<float>(0,0) = COMPRESSION_CONST*ALPHA_X;
	K.at<float>(0,1) = CAM_GAMMA;
	K.at<float>(0,2) = COMPRESSION_CONST*U_0;
	K.at<float>(1,1) = COMPRESSION_CONST*ALPHA_Y;
	K.at<float>(1,2) = COMPRESSION_CONST*V_0;
	K.at<float>(2,2) = 1.0f;
	//K << COMPRESSION_CONST*ALPHA_X, CAM_GAMMA, COMPRESSION_CONST*U_0,
	//             0, COMPRESSION_CONST*ALPHA_Y, COMPRESSION_CONST*V_0,
	//             0,0,1;

	Eigen::Matrix4f RT;
	RT <<  0,0,0, X_SHIFT,
			-1,cos(TURNING_ANGLE),-sin(TURNING_ANGLE),Y_SHIFT,
			0,sin(TURNING_ANGLE),cos(TURNING_ANGLE),Z_SHIFT,
			0,0,0,1;


	Mat rvec, tvec;
	solvePnP(comic_corners_object, comic_corners_image, K,
	                     DIST_COEFF, rvec, tvec, false, CV_P3P);



	return 0;
}
